var Fetch = require('whatwg-fetch');

const baseUrl = 'http://localhost:8080/api';

var service = {
    get: function (url) {
        return fetch(baseUrl + url, {credentials : 'include'})
            .then(function (response) {
                return response.json();
            });
    },

    post: function (url, json_body) {
        return fetch(baseUrl + url, {
            credentials : 'include',
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: json_body
        }).then(function (response) {
            return response.json();
        })
    },

    put: function (url, json_body) {
        return fetch(baseUrl + url, {
            credentials : 'include',
            method: 'put',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: json_body
        }).then(function (response) {
            return response.json();
        })
    },

    delete: function (url) {
        return fetch(baseUrl + url, {credentials: 'include', method: 'delete'})
            .then(function (response) {
                return response.json();
            })
    }

};

module.exports = service;
