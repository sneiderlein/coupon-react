var React = require('react');
var Actions = require('../reflux/login_actions.jsx');

var Login = React.createClass({
    
    getInitialState : function () {
        return {
            userType: 'company',
            userName: '',
            password: ''
        };
    },

    handleTypeChange : function(event)
    {
        this.setState({userType: event.target.value});

    },

    handleNameChange : function(event)
    {
        this.setState({userName: event.target.value});
    },

    handlePassChange : function(event)
    {
        this.setState({password: event.target.value});
    },

    handleFormSubmit : function () {
        if(this.validateForm())
        {

            

        }
        console.log('submitting the form!\n' + `type: ${this.state.userType}\n name: ${this.state.userName}\ntype: ${this.state.password}\n`);
        
        Actions.tryLoggingIn(JSON.stringify({
            userName: this.state.userName,
            userType: this.state.userType,
            password: this.state.password
        }));
    },

    validateForm : function () {

        return true;
    },
    
    render: function() {
        return (

           <div>
               <div>Please log iN!</div><br/>

               type: <select value={this.state.userType} onChange={this.handleTypeChange}>
               <option value="admin">admin</option>
               <option value="customer">customer</option>
               <option value="company" >company</option>
           </select>
               name: <input type="text" onChange={this.handleNameChange}/><br/>
               pass: <input type="text" onChange={this.handlePassChange}/><br/>
               <input type="button" onClick={this.handleFormSubmit}/>

           </div>
        );
    }
});

module.exports = Login;