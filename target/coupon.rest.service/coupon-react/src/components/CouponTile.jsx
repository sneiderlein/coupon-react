var React = require('react');

var CouponTile = React.createClass({

    randomBetween: function (min, max) {
        return (min + Math.ceil(Math.random() * max));
    },

    render: function () {
        var style = {
            transform: 'rotate(' + this.randomBetween(-5, 10) + 'deg)'
        };

        return (
            <div className="couponTile" style={style} onClick={this.props.childCallBack.bind(null, this.props.coupon)}>
                <a href="#">
                    <img src={this.props.coupon.imagePath}/>
                    <h5>{this.props.coupon.title}</h5>
                </a>
            </div>
        );
    }
});

module.exports = CouponTile;