var Reflux = require('reflux');
var Actions = require('./login_actions.jsx');
var http = require("../services/fetch");

var LoginStore = Reflux.createStore({
    listenables: [Actions],
    getLoggedInfo: function () {

        http.get('/login')
            .then(function (data) {
                console.log('from login store: ' + data);
                this.userInfo = data;
                this.doUpdate();
            }.bind(this));
    },

    tryLoggingIn: function (loginInfoAsJson) {

        console.log("we are posting!: " + loginInfoAsJson);

        http.post('/login', loginInfoAsJson)
            .then(function (response) {
                console.log(response);
                this.userInfo = response;
                this.doUpdate();
            }.bind(this));
        
    },

    logout: function () {
      http.delete('/login')
          .then(function (response) {
              console.log(response);
              this.userInfo = response;
              this.doUpdate();
          }.bind(this))
    },

    doUpdate: function () {
        this.trigger('change', this.userInfo);
    }
    
});

module.exports = LoginStore;