var Reflux = require('reflux');
var Actions = require('./gui_actions.jsx');

var GuiStore = Reflux.createStore({
    listenables: [Actions],
    filterCoupons: function (query)
    {
        this.filter = query;
        this.doUpdate();
    },
    doUpdate: function () {
        this.trigger('change', this.filter);
    }

});

module.exports = GuiStore;