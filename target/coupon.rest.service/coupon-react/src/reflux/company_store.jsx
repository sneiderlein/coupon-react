var Reflux = require('reflux');
var Actions = require('./company_actions.jsx');
var http = require("../services/fetch");

var CompanyStore = Reflux.createStore({

    listenables: [Actions], 

    getCompanyCoupons: function () {
        http.get('/company/coupon')
            .then(function (data) {
                console.log('from company coupon store: ' + data);
                this.companyCoupons = data;
                this.doUpdate();
            }.bind(this));
    },

    createNewCoupon: function () {

    },

    updateCoupon: function () {

        http.post('/company/coupon', couponAsJson)
            .then(function (response) {
                console.log(response);
                this.updatedCoupon = response;
                this.trigger('change', this.updatedCoupon);
            }.bind(this));
        

    },

    removeCoupon: function () {

    },
    doUpdate: function () {
        this.trigger('change', this.companyCoupons);
    }
});

module.exports = CompanyStore;