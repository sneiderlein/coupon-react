var Reflux = require('reflux');

var Actions = Reflux.createActions([
    'filterCoupons'
]);

module.exports = Actions;