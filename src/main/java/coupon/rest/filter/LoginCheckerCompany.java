package coupon.rest.filter;

import coupon.facade.CompanyFacade;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginCheckerCompany implements Filter
{
    public void destroy()
    {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException
    {
        System.out.println("Now filtering for company**********************************************************");
        //Cast
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        //Check if facade exists in the session. If not redirect to login page.
        HttpSession session = request.getSession();

        if(session == null || session.isNew() || session.getAttribute("facade") == null || !(session.getAttribute("facade") instanceof CompanyFacade))
        {
            System.out.println("Filter: Apparently not logged*********************************************");
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "Not logged in. You can't mess around here.");
            return;
        }

        System.out.println("Filter: If we're here, logged ***********************************************");
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException
    {

    }

}
