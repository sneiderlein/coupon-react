package coupon.rest.service;

import coupon.CouponSystem;
import coupon.exception.CouponException;
import coupon.facade.AdminFacade;
import coupon.facade.CompanyFacade;
import coupon.facade.CustomerFacade;
import coupon.fx2.MainController;
import coupon.rest.service.beans.FormWrapper;
import coupon.rest.service.beans.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("login")
public class LoginResource
{

    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public UserInfo loginWithJson(FormWrapper form, @Context HttpServletRequest request) throws CouponException
    {
        return this.login(form.getUserName(), form.getPassword(), form.getUserType(), request);
    }


    @POST
    @Produces("application/json")
    public UserInfo login(@FormParam("name") String name, @FormParam("pass") String pass, @FormParam("type") String type, @Context HttpServletRequest request) throws CouponException
    {
        //Check if parameters were sent
        if (name == null || pass == null || type == null)
        {
            throw new CouponException("No data or partial data was sent!");
        }

        //Check if already logged
        HttpSession session = request.getSession(true);
        if (session.getAttribute("facade") != null)
        {
            return getCurrentUser(request);
        }

        //Log in but check the type before
        switch (type)
        {
            case "admin":

                AdminFacade adminFacade = CouponSystem.instance.adminLogin(name, pass);
                session.setAttribute("facade", adminFacade);
                return getCurrentUser(request);

            case "customer":

                CustomerFacade customerFacade = CouponSystem.instance.customerLogin(name, pass);
                session.setAttribute("facade", customerFacade);
                return getCurrentUser(request);

            case "company":

                CompanyFacade companyFacade = CouponSystem.instance.companyLogin(name, pass);
                session.setAttribute("facade", companyFacade);
                return getCurrentUser(request);
        }

        //If we are here, something went wrong
        throw new CouponException("Something bad has happened!");
    }

    @GET
    @Produces("application/json")
    public UserInfo getCurrentUser(@Context HttpServletRequest request) throws CouponException
    {
        HttpSession session = request.getSession(false);

        System.out.println("getting current user!");

        if (session == null)
        {
            return new UserInfo(null, null, false, "not logged in!");
        }

        if (session.getAttribute("facade") == null)
        {
            return new UserInfo(null, null, false, "not logged in!");
        }

        if (session.getAttribute("facade") instanceof CompanyFacade)
        {
            return new UserInfo(
                    ((CompanyFacade) session.getAttribute("facade")).getCurrentCompany().getCompName(),
                    MainController.UserType.Company,
                    true, "n/a");
        }
        if (session.getAttribute("facade") instanceof CustomerFacade)
        {
            return new UserInfo(
                    ((CustomerFacade) session.getAttribute("facade")).getCurrentCustomer().getCustName(),
                    MainController.UserType.Customer,
                    true, "n/a");
        }

        if (session.getAttribute("facade") instanceof AdminFacade)
        {
            return new UserInfo(
                    "admin",
                    MainController.UserType.Admin,
                    true, "n/a");
        }

        throw new CouponException("Could not get current user", "LoginResource.getCurrentUser() -> no condition came back true!");
    }

    @DELETE
    public UserInfo logOut(@Context HttpServletRequest request)
    {
        HttpSession session = request.getSession(false);

        System.out.println("Logging out!!");

        if (session == null)
        {
            return new UserInfo(null, null, false, "not logged in!");
        } else
        {
            session.invalidate();
            return new UserInfo(null, null, false, "not logged in!");
        }
    }
}
