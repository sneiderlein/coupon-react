package coupon.rest.service;


import coupon.beans.Coupon;
import coupon.exception.CouponException;
import coupon.exception.LoginUnsuccessfulException;
import coupon.exception.ThinkAgainException;
import coupon.facade.CompanyFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("company")
public class CompanyResource
{
    @Context
    private HttpServletRequest request;

    @POST
    @Path("coupon")
    @Consumes("application/json")
    @Produces("application/json")
    public Coupon createCoupon(Coupon coupon) throws CouponException
    {
        //Try to get the facade
        CompanyFacade facade = FacadeAuth.checkAndGetCompanyFacade(request);
        //First check if authenticated
        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not authorized to be here. Please log in.");
        }

        Coupon c = facade.createCoupon(coupon);

        return c;
    }

    @DELETE
    @Path("coupon")
    @Consumes("application/json")
    @Produces("application/json")
    public Coupon deleteCoupon(Coupon coupon) throws CouponException
    {
        //Try to get the facade
        CompanyFacade facade = FacadeAuth.checkAndGetCompanyFacade(request);
        //Chech auth
        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not authorized to be here. Please log in.");
        }

        //Do the thing
        facade.removeCoupon(coupon);

        return coupon;
    }

    @PUT
    @Path("coupon")
    @Consumes("application/json")
    @Produces("application/json")
    public Coupon updateCoupon(Coupon coupon) throws CouponException
    {
        //Try to get the facade
        CompanyFacade facade = FacadeAuth.checkAndGetCompanyFacade(request);
        //Chech auth
        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not authorized to be here. Please log in.");
        }

        facade.updateCoupon(coupon);

        return coupon;
    }


    @GET
    @Produces("application/json")
    @Path("coupon/{id}")
    public Coupon getCoupon(@PathParam("id") long id) throws CouponException
    {
        //Try to get the facade
        CompanyFacade facade = FacadeAuth.checkAndGetCompanyFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not authorized to be here. Please log in.");
        }

        return facade.getCoupon(id);
    }


    @GET
    @Produces("application/json")
    @Path("coupon")
    public Collection<Coupon> getAllCoupons(@QueryParam("onlymy") boolean onlyMyCoupons,
                                            @QueryParam("bytype") String filterByType,
                                            @QueryParam("byname") String filterByName)
            throws CouponException
    {


        //Get the facade
        CompanyFacade facade = FacadeAuth.checkAndGetCompanyFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not authorized to be here. Please log in.");
        }

        return facade.getAllMyCoupons(); //TODO: implement it in the facade and the DAO's.
    }


}
