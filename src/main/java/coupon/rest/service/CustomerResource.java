package coupon.rest.service;

import coupon.beans.Coupon;
import coupon.exception.CouponException;
import coupon.exception.LoginUnsuccessfulException;
import coupon.facade.CustomerFacade;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("customer")
public class CustomerResource
{

    @Context
    private HttpServletRequest request;


    @Path("coupon/buy")
    @POST
    @Consumes("application/json")
    public Response purchaseCoupon(Coupon coupon) throws CouponException
    {
        //Try to get the facade, if exists
        CustomerFacade facade = FacadeAuth.checkAndGetCustomerFacade(request);
        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to be here, please log in", "facade came out null on purchaseCoupon()");
        }

        facade.purchaseCoupon(coupon);

        return Response.ok("Coupon was purchased successfully").build();
    }

    @GET
    @Path("coupon/{id}")
    @Produces("application/json")
    public Coupon getCoupon(@PathParam("id") long id) throws CouponException
    {
        //Try to get the facade, if exists
        CustomerFacade facade = FacadeAuth.checkAndGetCustomerFacade(request);
        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to be here, please log in", "facade came out null on purchaseCoupon()");
        }

        return facade.getCoupon(id);
    }

    @GET
    @Path("coupon")
    @Produces("application/json")
    public Collection<Coupon> getAllCoupons(@QueryParam("onlymy") boolean onlyMyCoupons,
                                            @QueryParam("bytype") String filterByType,
                                            @QueryParam("byname") String filterByName)
            throws CouponException
    {
        //TODO: implement filtering options!

        //Try to get the facade, if exists
        CustomerFacade facade = FacadeAuth.checkAndGetCustomerFacade(request);
        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to be here, please log in", "facade came out null on purchaseCoupon()");
        }

        return facade.getAllCoupons();
    }




}
