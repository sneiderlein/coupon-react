package coupon.rest.service.beans;

import coupon.fx2.MainController;

public class UserInfo
{

    private String userName;
    private MainController.UserType userType;
    private boolean logged;
    private String error;

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public UserInfo(String userName, MainController.UserType userType, boolean logged, String error)
    {
        this.userName = userName;
        this.userType = userType;
        this.logged = logged;
        this.error = error;
    }

    public UserInfo()
    {
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public MainController.UserType getUserType()
    {
        return userType;
    }

    public void setUserType(MainController.UserType userType)
    {
        this.userType = userType;
    }

    @Override
    public String toString()
    {
        return "UserInfo{" +
                "userName='" + userName + '\'' +
                ", userType=" + userType +
                '}';
    }

    public boolean isLogged()
    {
        return logged;
    }

    public void setLogged(boolean logged)
    {
        this.logged = logged;
    }

}
