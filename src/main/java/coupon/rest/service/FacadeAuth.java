package coupon.rest.service;

import coupon.Logger;
import coupon.facade.AdminFacade;
import coupon.facade.CompanyFacade;
import coupon.facade.CustomerFacade;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;

public class FacadeAuth
{



    public static CompanyFacade checkAndGetCompanyFacade(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        if(session == null || session.getAttribute("facade") == null || !(session.getAttribute("facade")instanceof CompanyFacade))
        {
            return null;
        }

        return (CompanyFacade) session.getAttribute("facade");
    }

    public static CustomerFacade checkAndGetCustomerFacade( HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        if(session == null || session.getAttribute("facade") == null || !(session.getAttribute("facade")instanceof CustomerFacade))
        {
            return null;
        }

        return (CustomerFacade) session.getAttribute("facade");
    }

    public static AdminFacade checkAndGetAdminFacade(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        if(session == null || session.getAttribute("facade") == null || !(session.getAttribute("facade")instanceof AdminFacade))
        {
            return null;
        }

        return (AdminFacade) session.getAttribute("facade");
    }

}
