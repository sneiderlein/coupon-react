package coupon.rest.service;

import coupon.beans.Company;
import coupon.beans.Coupon;
import coupon.beans.Customer;
import coupon.exception.CouponException;
import coupon.exception.LoginUnsuccessfulException;
import coupon.facade.AdminFacade;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("admin")
public class AdminResource
{
    @Context
    private HttpServletRequest request;

    /*
    Company methods
     */

    @POST
    @Path("company")
    @Consumes("application/json")
    public Response createCompany(Company company) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.createCompany(): AdminFacade came out null!");
        }

        facade.createCompany(company);

        return Response.ok("Company was created successfully").build();
    }

    @DELETE
    @Path("company")
    @Consumes("application/json")
    public Response removeCompany(Company company) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.removeCompany(): AdminFacade came out null!");
        }

        facade.removeCompany(company);

        return Response.ok("Company was deleted successfully").build();
    }

    @PUT
    @Path("company")
    @Consumes("application/json")
    public Response updateCompany(Company company) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.updateCompany(): AdminFacade came out null!");
        }

        facade.updateCompany(company);

        return Response.ok("Company was updated successfully").build();
    }

    @GET
    @Path("company/{id}")
    @Produces("application/json")
    public Company getCompany(@PathParam("id") long id) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.getCompany(): AdminFacade came out null!");
        }

        return facade.getCompany(id);
    }

    @GET
    @Path("company")
    @Produces("application/json")
    public Collection<Company> getAllCompanies() throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.getAllCompanies(): AdminFacade came out null!");
        }

        return facade.getAllCompanies();
    }

    /*
    Customer methods
     */

    @POST
    @Path("customer")
    @Consumes("application/json")
    public Response createCustomer(Customer customer) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.createCustomer(): AdminFacade came out null!");
        }

        facade.createCustomer(customer);

        return Response.ok("Customer was created successfully").build();
    }

    @DELETE
    @Path("customer")
    @Consumes("application/json")
    public Response removeCustomer(Customer customer) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.removeCustomer(): AdminFacade came out null!");
        }

        facade.removeCustomer(customer);

        return Response.ok("Customer was deleted successfully").build();
    }

    @PUT
    @Path("customer")
    @Consumes("application/json")
    public Response updateCustomer(Customer customer) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.updateCustomer(): AdminFacade came out null!");
        }

        facade.updateCustomer(customer);

        return Response.ok("Customer was updated successfully").build();
    }

    @GET
    @Path("customer/{id}")
    @Produces("application/json")
    public Customer getCustomer(@PathParam("id") long id) throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.getCustomer(): AdminFacade came out null!");
        }

        return facade.getCustomer(id);
    }

    @GET
    @Path("customer")
    @Produces("application/json")
    public Collection<Customer> getAllCustomers() throws CouponException
    {
        //Try to get the facade
        AdminFacade facade = FacadeAuth.checkAndGetAdminFacade(request);

        if (facade == null)
        {
            throw new LoginUnsuccessfulException("You are not allowed to mess around here. Please log in as Admin.", "AdminResource.getAllCustomers(): AdminFacade came out null!");
        }

        return facade.getAllCustomers();
    }
}
