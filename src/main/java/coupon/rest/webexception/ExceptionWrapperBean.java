package coupon.rest.webexception;

public class ExceptionWrapperBean
{

    private String message;

    public ExceptionWrapperBean(String message)
    {
        this.message = message;
    }

    public ExceptionWrapperBean()
    {
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    @Override
    public String toString()
    {
        return "ExceptionWrapperBean{" +
                "message='" + message + '\'' +
                '}';
    }
}
