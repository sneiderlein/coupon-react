package coupon.rest.webexception;

import coupon.exception.CouponException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CouponWebException implements ExceptionMapper<CouponException>
{
    @Override
    public Response toResponse(CouponException e)
    {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(new ExceptionWrapperBean(e.getMessage())).build();
    }
}
